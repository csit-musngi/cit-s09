package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
@RestController
@RequestMapping("/greeting")
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	ArrayList<String> users = new ArrayList<>();
	@GetMapping("/enroll")
	public String user(@RequestParam(value="user", defaultValue = "Tolits")String user) {
		users.add(user);
		return String.format("Thank you for enrolling, %s!", user);
	}
	@GetMapping("/getEnrollees")
	public String getEnroll(){
		return String.format(String.valueOf(users));
	}

	@GetMapping("/nameage")
	public String nameAge(@RequestParam(value="name", defaultValue = "Tolits")String name, @RequestParam(value="age", defaultValue = "12") int age){
	return String.format("Hello %s! My age is %s.", name, age);
	}

	@GetMapping("/courses/{id}")
	public String course(@PathVariable ("id") String id){
		if(id.equals("java101")){
			return String.format("java 101, MWF 8:00 AM-11:00 AM, Price: PHP 3000");
		}
		else if(id.equals("sql101")){
			return String.format("SQL 101, TTH 1:00 PM-4:00 PM, Price: PHP 2000");

		}
		else if(id.equals("javaee101")){
			return String.format("java EE 101, MWF 1:00 PM-4:00 PM, Price: PHP 3500");

		}
		return String.format("Course cannot be found.");
	}
}
